SHELL := /bin/bash
include config.mk
PWD=$(shell pwd)
STATUS=0

.ONESHELL:
install-ere:
	mkdir -p ${INFRA}/${ERE_DIR}
	rm -rf ${INFRA}/${ERE_DIR}
	mkdir -p ${INFRA}/${ERE_DIR}
	cd ${INFRA}/${ERE_DIR}
	wget ${ERE_INFRA_LOC}
	tar zxvf *.gz
	rm -rf *.gz

copy-ere:
	mkdir -p ${EXP_BUILD_DIR}/js
	rsync  -a ${INFRA}/${ERE_DIR}/${ERE}/apis/build/code/runtime/js/ ${EXP_BUILD_DIR}/js/
	rsync  -a ${INFRA}/${ERE_DIR}/${ERE}/loader/build/code/runtime/libs/ ${EXP_BUILD_DIR}/libs/
	rsync  -a ${INFRA}/${ERE_DIR}/${ERE}/loader/build/code/runtime/js/ ${EXP_BUILD_DIR}/js/
	rsync  -a ${INFRA}/${ERE_DIR}/${ERE}/renderers/build/code/runtime/js/ ${EXP_BUILD_DIR}/js/
	rsync  -a ${INFRA}/${ERE_DIR}/${ERE}/styles/build/code/runtime/ ${EXP_BUILD_DIR}/


clean-ere:
	rm -rf ${INFRA}/${ERE_DIR}
